{{- define "ui.fullname" -}}
{{- printf "%s-%s" .Release.Name .Chart.Name}}
{{- end -}}
{{- define "ingress.name" -}}
{{- printf "%s-%s-%s-%s" .Release.Name .Chart.Name .Release.Namespace "ingress"}}
{{- end -}}
{{- define "ui.servicemon" -}}
{{- printf "%s-%s-%s" .Release.Name .Chart.Name "servicemonitor"}}
{{- end -}}
{{- define "cert.name" -}}
{{- printf "%s-%s" .Release.Name .Values.ingress.certName}}
{{- end -}}
{{- define "issuer.name" -}}
{{- printf "%s-%s" .Release.Name .Values.ingress.issuer}}
{{- end -}}
{{- define "domain.name" -}}
{{- printf "%s.%s" .Release.Name .Values.ingress.host}}
{{- end -}}
